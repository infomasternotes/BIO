# Bioinformatica

## Downloads
Se non ti interessa contribuire, ti sarà più comodo scaricare direttamente il file pdf degli appunti piuttosto che clonare il repository.  
[Download diretto del pdf dell'ultima release](https://gitlab.com/infomasternotes/BIO/-/jobs/artifacts/master/raw/BIO.pdf?job=pdf)  
[Download diretto del pdf dell'ultima beta](https://gitlab.com/infomasternotes/BIO/-/jobs/artifacts/develop/raw/BIO.pdf?job=pdf)

## Contribuire 
Se noti alcune cose che ti sembrano poco chiare o scorrette puoi apportare le tue modifiche e fare una *merge request*.

### Build
La build è automatizzata con **make**. Se utilizzi un editor LaTeX, prima dovrai eseguire *make*, che provvederà al download di alcune immagini.  
Vengono usati il compilatore **pdflatex** e i pacchetti della distribuzione **texlive**. Per quanto riguarda i download, viene usato **wget**.

#### Gitlab CI
Una volta caricato il repository su GitLab, il processo di build viene eseguito in un container Docker. 
Se non hai le dipendenze necessarie sulla tua macchina e non vuoi scaricarle per apportare piccole modifiche, puoi sfruttare questo servizio.

### File System
Nella directory principale sono presenti il file tex principale *BIO.tex*, il *makefile*, il file di configurazione per la CI *.gitlab-ci.yml*, questo *README.md* e il file *.gitignore*. 
Nella cartella *tex* sono presenti i file tex dei capitoli, nella cartella *img* sono presenti le immagini scaricate e nella cartella *cod* i file di pseudocodice.

### Figure
Se vuoi integrare con delle figure, per favore includile come mostrato sotto per coerenza rispetto a quelle già presenti  
`\begin{figure} \centering
	\caption{<caption>}
	\label{fig:<figlabel>}
	<image>
\end{figure}`  
Sono molto gradite immagini vettoriali (meglio se in .pdf o generate direttamente con codice LaTeX)
