TEXC=pdflatex
TEXF= 
TARGET=BIO
RM=rm -rf
OUTEXT=pdf
DELEXT= aux log out synctex.gz* toc bbl blg
DEPFILES= tex img/gwas.pdf img/cadd.pdf img/deepsea.jpg img/hypersmurf.png img/hycomparePRC.jpg img/hycompareIMB.jpg img/roc.png img/prcroc.png

$(TARGET).pdf: $(TARGET).tex $(DEPFILES)
	make build
	make clean

img/gwas.pdf:
	wget ftp://ftp.ebi.ac.uk/pub/databases/gwas/timeseries/current/GWAS_Catalog_diagram.pdf -O img/gwas.pdf
img/cadd.pdf:
	mkdir .tmp
	wget https://homes.di.unimi.it/valentini/SlideCorsi/Bioinformatica1617/ML-PerMed-small.pdf -O .tmp/slide.pdf
	printf "\\" > .tmp/cadd.tex
	printf "documentclass[12pt]{report}\n" >> .tmp/cadd.tex
	printf "\\" >> .tmp/cadd.tex
	printf "usepackage{pdfpages}\n" >> .tmp/cadd.tex
	printf "\\" >> .tmp/cadd.tex
	printf "begin{document}\n" >> .tmp/cadd.tex
	printf "\\" >> .tmp/cadd.tex
	printf "includepdf[pages={22}]{.tmp/slide.pdf}\n" >> .tmp/cadd.tex
	printf "\\" >> .tmp/cadd.tex
	printf "end{document}\n" >> .tmp/cadd.tex
	pdflatex -output-directory .tmp cadd.tex
	mv .tmp/cadd.pdf img/cadd.pdf
	rm -rf .tmp
img/deepsea.jpg:
	wget https://media.nature.com/m685/nature-assets/nmeth/journal/v12/n10/images/nmeth.3547-F1.jpg -O img/deepsea.jpg
img/hypersmurf.png:
	wget https://www.researchgate.net/profile/Max_Schubach/publication/317489171/figure/fig1/AS:504098978844672@1497197970536/A-schematic-representation-of-the-hyperSMURF-method-HyperSMURF-divides-the-majority.png -O img/hypersmurf.png
img/hycomparePRC.jpg:
	wget https://media.springernature.com/m685/springer-static/image/art%3A10.1038%2Fs41598-017-03011-5/MediaObjects/41598_2017_3011_Fig4_HTML.jpg -O img/hycomparePRC.jpg
img/hycompareIMB.jpg:
	wget https://media.springernature.com/lw900/springer-static/image/art%3A10.1038%2Fs41598-017-03011-5/MediaObjects/41598_2017_3011_Fig6_HTML.jpg -O img/hycompareIMB.jpg
img/roc.png:
	wget https://www.researchgate.net/profile/Nicholas_Czarnek/publication/320410861/figure/fig3/AS:551041818206213@1508390015497/Generation-of-ROC-curves-The-plot-on-the-left-shows-the-probability-density-functions.png -O img/roc.png
img/prcroc.png:
	wget https://www.dropbox.com/s/pwumsb4jcvam2cz/prcroc.png?dl=1 -O img/prcroc.png
.PHONY: compile build clean reset
compile:
	$(TEXC) $(TEXF) $(TARGET).tex
build:
	for i in 1 2 3 ; do \
		make compile ; \
	done
clean:
	for ext in $(DELEXT) ; do \
		$(RM) $(TARGET).$$ext ; \
	done
reset:
	$(RM) $(TARGET).$(OUTEXT)
	make clean
