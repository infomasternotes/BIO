\chapter{Metodi di Apprendimento Automatico}
I metodi di \textit{machine learning} sono algoritmi di tipo \textit{data driven}: si contrappongono alla programmazione tradizionale perché, anziché produrre un output da degli input e da un programma, producono un programma (un modello) a partire da input e output.

Sono, quindi, tre le principali componenti di un sistema di apprendimento automatico: la forma di rappresentazione del modello, la funzione di valutazione delle performance del modello prodotto e l'algoritmo di ottimizzazione che implementa l'apprendimento. Inoltre, è molto importante avere un formato di rappresentazione dei dati che sia adatto al sistema di apprendimento scelto.

Per quanto riguarda l'apprendimento, si possono distinguere algoritmi di apprendimento supervisionato, non-supervisionato, semi-supervisionato e apprendimento con rinforzo. Tipicamente, i problemi della biologia computazionale sono affrontati con tecniche supervisionate o semi-supervisionate in quanto si parte da una base di dati più o meno etichettati.

\subsubsection{Classe di Ipotesi}
La classe di ipotesi è l'insieme delle soluzioni ammesse dalle ipotesi effettuate alla base della specifica di un sistema di apprendimento: in particolare, il modello scelto determina la classe di ipotesi. L'apprendimento consisterà, quindi, nel cercare il migliore modello per il problema affrontato all'interno della classe di ipotesi.

\subsubsection{Ottimizzazione}
L'ottimizzazione può sfruttare diverse tecniche, in particolare si distinguono tecniche di ottimizzazione combinatoria, ottimizzazione convessa (discesa del gradiente) e ottimizzazione vincolata (programmazione lineare).

\subsubsection{Valutazione}
La valutazione consiste nel misurare come si comporta il sistema con esempi che non ha mai visto in precedenza: quanto è buono un sistema può essere determinato calcolando la precisione, l'errore quadratico medio, l'entropia, l'area sotto la curva ROC o PRC, \dots 

\subsubsection{Tipi di Apprendimento}
L'apprendimento può essere:\begin{itemize}
	\item Apprendimento Supervisionato: all'algoritmo di apprendimento viene fornito un insieme di coppie input/output e il sistema deve imparare la relazione tra di essi
	\item Apprendimento Non-supervisionato: all'algoritmo di apprendimento viene fornito un insieme input e il sistema deve imparare alcune proprietà dei dati (es. clustering)
	\item Apprendimento Semi-supervisionato: solo alcuni dati di input sono forniti insieme all'output corrispondente
	\item Apprendimento con Rinforzo: il sistema è un agente ed impara dal feedback che ottiene dall'interazione con l'ambiente
\end{itemize}

\section{Funzione di Perdita}
La funzione di perdita è una funzione che indica quanto la stima in uscita da un modello sia errata: l'apprendimento diventa, quindi, un problema di ottimizzazione il cui obiettivo è la minimizzazione della funzione di perdita.

Il nostro obiettivo, però, è minimizzare la perdita attesa su esempi sui quali il sistema non è stato addestrato: per stimare questo valore, separiamo i dati in \textit{training set} (dati usati per l'addestramento) e \textit{test set} (dati usati per la valutazione).

\subsection{Funzione}
La funzione scelta dipende dal problema affrontato: per problemi di classificazione l'errore consiste nello sbagliare la predizione e la funzione naturale è la funzione di perdita zero-uno
$$ loss(y,\hat{y}) := \mathbb{I}\graffe{y=\hat{y}} $$
Per problemi di regressione, invece, la perdita è tanto maggiore quanto il valore predetto si discosta da quello misurato e la funzione più naturale è l'errore quadratico
$$ loss(y,\hat{y}) := (y-\hat{y})^2 $$

\subsection{Rischio Empirico}
Il \textit{training error} è la media campionaria della funzione di perdita 
valutata sul \textit{training set} ed è la funzione che l'algoritmo minimizza.
$$ f^* = \argmin_{\hat{f}\in\mathcal{H}} \frac{1}{n} \sum_{i=1}^{N} loss(y_i, \hat{f}(x_i)) $$
Dove $\graffe{(x_i,y_i)}_{i=1}^n$ è il training set e $\mathcal{H}$ è la classe di ipotesi. Il predittore $f^*$ è detto minimizzatore del rischio empirico.

\subsection{Rischio Statistico}
Il rischio statistico (o rischio atteso) è il valore atteso della perdita sull'universo dei dati e il minimizzatore di tale rischio è il minimizzatore del rischio statistico
$$ f^* = \argmin_{\hat{f}\in\mathcal{H}}  \ \mathbb{E}\left[loss(y_i, \hat{f}(x_i))\right] $$
Per ottenere questo valore atteso dovremmo conoscere la probabilità congiunta $p( X , Y )$, da cui avremmo che
$$ f^* = \argmin_{\hat{f}\in\mathcal{H}} \int_{x\in X}\int_{y\in Y} loss(y,\hat{f}(x))\,p(X=x,Y=y)\ dx\ dy $$
Se conoscessimo tale distribuzione, l'apprendimento non sarebbe necessario. In generale, non la conosciamo, quindi, stimiamo il rischio atteso con il rischio empirico valutato sul \textit{test set}.

\subsection{Decomposizione dell'errore}
L'errore che compie un predittore può essere decomposto in due componenti. L'errore strutturale (o errore di \textit{bias}) è l'errore introdotto dalla classe di ipotesi: ovvero, è l'errore che compie il miglior predittore all'interno della classe di ipotesi
$$ \mathbb{E}\left[loss(y_i, f^*(x_i))\right] = \min_{\hat{f}\in\mathcal{H}}  \ \mathbb{E}\left[loss(y_i, \hat{f}(x_i))\right] $$
Invece, l'errore di approssimazione (o errore di varianza) è l'errore che compie il predittore ottenuto rispetto al miglior predittore della classe
$$\mathbb{E}\left[loss(y_i, \hat{f}(x_i))\right] - \mathbb{E}\left[loss(y_i, f^*(x_i))\right]$$
Come è evidente, l'errore totale è dato dalla somma delle due componenti.

\section{Stima dell'Errore di Generalizzazione}
Ciò che interessa è minimizzare non il rischio empirico sull'insieme dei dati disponibili, ma il rischio atteso sull'intero dominio dei dati: occorre un metodo per la stima di tale errore.
La teoria statistica dell'apprendimento di Vapnik fornisce una maggiorazione del rischio statistico
$$ R(\omega) = R_{emp}(\omega) + \Phi\left(\frac{h}{m}\right) $$
Questa quantità è il rischio empirico maggiorato di una confidenza $\Phi$ che dipende dalla complessità $h$ della classe di ipotesi e dalla cardinalità $m$ del training set. Una strada, quindi, è quella di analizzare a livello teorico il rischio atteso, ma un'altra è quella della stima sperimentale.

Per scegliere il predittore migliore nella classe in fase di training, dividiamo il training set in due insiemi: \textit{training set} e \textit{validation set}. Il training set è l'insieme utilizzato per l'apprendimento e il validation set l'insieme utilizzato per stimare l'errore di generalizzazione in fase di training: verrà scelto il modello con il minor \textit{validation error}, che sarà riaddestrato sulla totalità del training set. L'errore di generalizzazione viene, a questo punto, stimato con la perdita sul test set del predittore ottimo per il validation set.

La cross-validazione, invece, non è una tecnica per determinare il predittore migliore, ma per stimare l'errore di generalizzazione tipico di un algoritmo: esso viene eseguito con diverse configurazioni di training set e validation set, ottenendo predittori diversi. La media dei validation error costituisce una stima dell'errore di generalizzazione del predittore.

In ogni caso, training set, validation set e test set devono essere divisi in modo che siano indipendenti e identicamente distribuiti (tipicamente si effettuano partizioni causali).

Esistono metodi alternativi alla cross-validazione, come i metodi di \textit{bootstrap} e i metodi \textit{out-of-the-bag}.

\subsection{Hold Out}
Nella validazione \textit{hold-out}, i dati di training vengono divisi in due insiemi, che costituiranno il training set ed il validation set. Questa stima sarà fortemente dipendente da come sono stati partizionati i dati e, inoltre, sottrae dati al training: è possibile arginare questi problemi facendo un hold out ripetuto su bipartizioni casuali del dataset (\textit{random subsampling}).

\subsection{Cross-validation}
La cross-validazione può essere effettuata tramite \textit{random subsampling} o tramite \textit{k-fold cross-validation}.
K-fold cross-validation consiste nel dividere i dati di training in $k$ insiemi indipendenti e identicamente distribuiti: ad ogni iterazione $i\in[1,k]$ l'insieme $i$-esimo viene sottratto dai dati di training e utilizzato come insieme di validazione, mentre i restanti $k-1$ insiemi sono utilizzati come training set. Anche in questo caso la stima dell'errore di generalizzazione è data dalla media degli errori di validazione: la differenza sostanziale è che in questo caso ogni esempio viene considerato una e una sola volta come caso di test (e $k-1$ come dato di training).

Nel caso limite in cui $k$ è la dimensione del training set, k-fold cross-validation prende il nome di \textit{leave-one-out cross-validation} (LOOCV): ad ogni iterazione il validation set è composto da un solo esempio, mentre tutti gli altri compongono il test set.

In generale, valori elevati di $k$ restituiscono una stima più realistica dell'errore di generalizzazione, ma richiedono un tempo maggiore per la procedura di cross-validazione. Per cui LOOCV viene utilizzato soprattutto su dataset molto piccoli, mentre random subsampling su dataset molto ampi.
\newpage
\section{Algoritmi di Apprendimento Supervisionato}
\subsection{KNN}
\textit{K-Nearest-Neighbours} è un algoritmo di classificazione che, dato un training set $D\in X\times Y$ e una funzione di distanza $d:X \rightarrow \mathbb{R}$ associa ad una nuova istanza $x$ l'etichetta della maggior parte dei $k$ elementi più vicini a $x$ in $D$
$$ f(x) = \argmax_{y\in Y} \left|\graffe{ (x_i,y_i)\in D : y=y_i \land \left|\graffe{(x_j,y_j)\in D} : d(x_j) \leq d(x_i) \right| \leq k} \right| $$
KNN è molto costoso: in termini di spazio perché richiede di avere in memoria tutto il dataset, e in termini di tempo, perché richiede di calcolare la distanza da ogni punto del dataset. Inoltre, per valori bassi di $k$ è molto sensibile al rumore, mentre per valori alti diventa ancora più costoso.

Inoltre, soffre della \textit{curse of dimensionality}: in spazi ad alta dimensionalità i punti risultano più sparsi e la distanza risulta meno significativa. Sono necessarie tecniche di \textit{dimensionality reduction} o di \textit{feature selection}.

\subsection{Percettrone}
Il percettrone lineare effettua una sommatoria delle feature di input $x$ pesata secondo un vettore di pesi $w$, dopodiché la somma viene sogliata calcolando il segno della differenza rispetto ad un bias $\mu$
$$ y = \text{sign}\left( \sum_{i=1}^{m}w_i\,x_i - \mu \right) = \text{sign}(w \cdot x - \mu ) $$
Ponendo $x_0 = -1$ e $w_0 = \mu$, la notazione diventa più compatta
$$ y = \text{sign}(w \cdot x) $$
Il vettore dei pesi $w$ rappresenta un iperpiano separatore (i punti $x : w\cdot x =0$) tra le etichette positive e negative ed è ciò che l'algoritmo deve apprendere.

Al posto della funzione segno, è possibile utilizzare altre funzioni di attivazione, come la funzione sigmoide logistica 
$$ \sigma(x) = \frac{1}{1+e^{-x}} $$
Il vettore dei pesi può essere appeso con l'algoritmo di discesa del gradiente: aggiorniamo iterativamente il valore del vettore dei pesi nella direzione di massima diminuzione dell'errore (nella direzione del'antigradiente). Definito l'errore 
$$ E(w) = \sum_{t=1}^{m} loss(\sigma(wx^{(t)}),y^{(t)}) $$
L'aggiornamento iterativo è
$$ w \leftarrow w - \eta\cdot\nabla E(w) $$
dove $\eta$ è il parametro di \textit{learning rate}. Il gradiente dell'errore è 
$$ \nabla E(w) = \left( \frac{\partial E(w)}{\partial w_i} \right)_{i=1}^{n} = \left( \frac{\partial \left[ \sum_{t=1}^{m} loss(\sigma(wx^{(t)}),y^{(t)}) \right] }{\partial w_i} \right)_{i=1}^{n} $$
Per $\sigma(x) = x$ e $loss(\hat{y},y)=\nicefrac{1}{2}(\hat{y}-y)^2$
$$ \nabla E(w) = \left( \frac{\partial \left[ \sum_{t=1}^{m} \nicefrac{1}{2}\left(wx^{(t)}-y^{(t)}\right)^2 \right] }{\partial w_i} \right)_{i=1}^{n} = \left( \sum_{t=1}^{m} x_i^{(t)}\left(wx^{(t)}-y^{(t)}\right) \right)_{i=1}^{n} $$
L'algoritmo di discesa del gradiente è
\begin{lstlisting}
initialize w
repeat
	for (x,y) $\in$ D
		$\Delta$w := $\eta$ (y - w $\cdot$ x) x
		w := w + $\Delta$w
until convergence
\end{lstlisting}
Condizioni di convergenza possono essere la convergenza dell'errore ($E(w) < \tau$) o dell'aggiornamento ($\Delta w < \tau$), oppure il raggiungimento di un numero di iterazioni prefissate.

Per problemi di classificazione multiclasse, possono essere utilizzati k percettroni: l'input rimane un vettore $x$, i pesi sono la matrice $W$ e le uscite il vettore $y$, dove
$$ y = Wx\ :\ y_k = \sum_{j=1}^{k} W_{k,j}\,x_j $$
Questo modello è chiamato \textbf{percettrone a singolo strato}: la classe predetta può essere inferita osservando il massimo di $y$ (\textit{winner takes all}), oppure si può esprimere la probabilità di ogni classe con un operazione come il \textit{softmax}
$$\text{softmax}(y) = \frac{e^y}{\left|\left| e^{y} \right|\right|_1} = \left(\frac{e^{y_j}}{ \sum_{h=1}^{k}e^{y_h} }\right)_{j=1}^{k} $$

\subsection{Multi-Layer Perceptron}
I percettroni a singolo strato possono apprendere solo funzioni lineari (e classi linearmente separabili): per apprendere funzioni non-lineari si utilizzano modelli composti da più strati di percettroni, le reti neurali. 

Il percettrone multistrato è composto da più \textit{layer} in sequenza e ogni layer è composto da più percettroni: il primo layer ha come input il vettore di ingresso, mentre il layer $i$-esimo ha come ingresso il layer precedente. Ogni strato ha una matrice di pesi associata
$$ h_i = \begin{cases}
W_i\ x & i = 1 \\
W_i\ h_{i-1} & i > 1
\end{cases} $$
Il vettore $x$ è detto \textit{input layer}, l'ultimo strato è detto \textit{output layer} e i rimanenti strati intermedi sono detti \textit{hidden layer}. Le reti neurali sono dette profonde (\textit{deep neural networks}) tipicamente quando hanno più di due strati (più di uno strato nascosto, l'input non conta come strato).

In questo caso non è possibile utilizzare l'algoritmo di discesa del gradiente: l'algoritmo di training per le reti neurali è l'algoritmo di \textit{back-propagation} (o algoritmo di retropropagazione dell'errore).

Alcune tecniche di regolarizzazione utilizzate sono il \textit{weight decay} (la norma $L^2$ dei pesi viene aggiunto alla funzione di perdita, è una regolarizzazione Tikhonov o \textit{ridge regression}) e il \textit{dropout} (mettere un peso a zero con una probabilità fissata).

\subsection{CNN}
Le \textit{convolutional neural networks} sono reti neurali in cui ogni strato viene ottenuto, non dal prodotto matrice-vettore dei pesi e dello strato precedente, ma dalla loro convoluzione
$$ y = w \ast x\ :\ y_i = \sum_{h=1}^{k} w_h\ x_{i-h} $$
Il vettore di pesi viene chiamato \textit{kernel}. Le funzioni di convoluzione implementate sono anche diverse da quella ortodossa (detta \textit{flipped-kernel}): ad esempio, può essere la funzione di cross-correlazione (senza rovesciamento del kernel, ma non commutativa). Gli strati convoluzionali possono implementare anche il padding (senza padding la lunghezza degli strati diminuisce inesorabilmente).

Un primo vantaggio delle CNN contro i MLP è che hanno meno parametri: i kernel sono generalmente piccoli, quindi occupano meno memoria, sono più semplici da apprendere (sono meno parametri) e implementano la sparsità delle connessioni (ogni strato è connesso solo al proprio \textit{campo recettivo}) propria delle reti neurali naturali. I parametri, oltre a essere meno, sono anche gli stessi per ogni neurone dello stesso strato, ciò che cambia è la sezione di input che viene considerata: questo fenomeno è detto \textit{parameter sharing}. In questo modo, le convoluzioni sono considerabili come un prodotto per una matrice di pesi con i vincoli enunciati di sparsità e di uguaglianza dei parametri e, quindi, possono essere considerate una regolarizzazione dei MLP.

Una proprietà delle CNN che deriva dall'utilizzo dell'operazione di convoluzione è che le trasformazioni da uno strato al successivo sono \textit{equivarianti per traslazione}: una traslazione nel vettore di input corrisponde a una stessa traslazione nel vettore di output.

Le CNN possono avere input di arbitrarie dimensioni se si gestisce il \textit{pooling} per ottenere questa proprietà, oppure utilizzando delle \textit{fully convolutional networks}.

\subsubsection{Layers}
Gli strati di una rete convoluzionale sono complessi. Sono composti da:\begin{itemize}
	\item Stadio convolutivo: è lo stadio che implementa la convoluzione dell'input per un kernel
	\item Stadio non-lineare: è lo stadio che applica la funzione di attivazione. La funzione di attivazione più comune nelle reti convoluzionali è la funzione ReLU $$ \text{ReLU}(x) = \begin{cases}
	x & x \geq 0\\
	0 & x < 0
	\end{cases} $$
	\item Stadio di \textit{pooling}: questo stadio calcola delle statistiche locali sull'output. Lo stadio di pooling più comune nelle reti convoluzionali è il \textit{max pooling}, che restituisce il massimo di finestre dell'output: questo consente di avere un output invariante rispetto a piccole fluttuazioni dell'input
\end{itemize}
Alternativamente, alcune tassonomie considerano strati semplici ed ognuno degli stadi sopra riportati costituisce uno strato a sé.

Ogni \textit{layer} convolutivo può avere più kernel: in questo modo la rete può apprendere in parallelo più feature. Inoltre, fare pooling sui risultati di più kernel permette alla rete di apprendere delle invarianze: ad esempio, nel riconoscimento di immagini è utile apprendere l'invarianza alla rotazione.

\subsubsection{Convoluzioni}
Come abbiamo detto, le convoluzioni implementate non sempre sono uguali alla convoluzione matematica (spesso si usa la cross-correlazione). Esistono varie versioni della convoluzione.

La \textit{strided convolution} non calcola il risultato della convoluzione per tutti gli indici, ma solo 1 ogni $s$, che chiamiamo lo \textit{stride} della convoluzione: questo è un metodo molto efficiente per ottenere downsampling.

La \textit{local connection} può essere vista come una convoluzione senza parameter sharing: ogni neurone è influenzato solo dal proprio campo recettivo nello strato precedente, ma i pesi cambiano da neurone a neurone. È anche chiamata \textit{unshared convolution}. Non garantisce l'equivarianza per traslazione, ma permette di apprendere in un solo strato kernel diversi che dipendono dalla regione dell'input a cui sono applicati.

La \textit{tiled convolution} è una via di mezzo tra convoluzione e local connection: viene appreso un set di kernel che si ripetono. Se lo strato sta apprendendo $n$ kernel, il neurone $i$-esimo userà il kernel $[i]_n$\,.

Nel caso di input di alta dimensionalità, le convoluzioni possono essere rese più efficienti effettuando il filtraggio nel dominio delle frequenze: infatti, se il kernel ha dimensione $k$ e l'input $m$, la convoluzione ha complessità $O(k\cdot m)$, mentre la procedura FFT, prodotto elemento-per-elemento e IFFT ha complessità $O(m\,\log{m})$\,+\,$O(m)$\,+\,$O(m\,\log{m})$\,=\,$O(m\,\log{m})$\,.

\subsection{SVM}
\textit{Support Vector Machine} è un algoritmo di apprendimento che produce il classificatore lineare dai margini massimali: nel caso di classi linearmente separabili, ciò vuol dire che tra gli infiniti iperpiani separatori viene determinato quello che massimizza la distanza dai punti di entrambe le classi.

\noindent
Dato un vettore $w$, l'iperpiano definito da quel vettore è
$$ S(w,b):=\graffe{ x \in \mathbb{R}^N : w^Tx + b = 0 } $$
Per avere il termine di bias incluso nel vettore $w$, fissiamo $x_1 := 1$
$$ S(w):=\graffe{ x \in \graffe{1}\times\mathbb{R}^N \ |\  w^Tx = 0 } $$
Se il margine dell'iperpiano ha larghezza $\gamma$, vuol dire che un punto $x_m$ è esattamente sul margine se
$$ \exists x_p\in S(w) : x_m = x_p + \frac{w}{\left|\left|w\right|\right|}\gamma $$
Dove $\nicefrac{w}{\left|\left|w\right|\right|}$ è in effetti il versore normale all'iperpiano. Allora, data $f(x):=w^Tx$, vale 
$$ f(x_m) = w^Tx_m = w^T\left( x_p + \frac{w}{\left|\left|w\right|\right|}\gamma \right) = w^Tx_p + \frac{w^Tw}{\left|\left|w\right|\right|}\gamma $$
Ma $ x_p\in S(w) \Rightarrow w^Tx_P = 0 $, quindi 
$$f(x_m) = \frac{\left|\left|w\right|\right|^2}{\left|\left|w\right|\right|}\gamma = \left|\left|w\right|\right|\gamma $$
Chiameremo tale misura il \textit{margine funzionale}. Chiameremo \textit{il margine geometrico}
$$ \gamma = \frac{f(x_m)}{\left|\left|w\right|\right|} $$
L'iperpiano canonico è ottenuto normalizzando l'iperpiano rispetto al margine funzionale
$$ w_c = \frac{w}{f(x_m)} = \frac{w}{\left|\left|w\right|\right|\gamma} $$
$$ f_c(x) = w_c^Tx = \frac{w^T}{\left|\left|w\right|\right|\gamma}x = \frac{f(x)}{\left|\left|w\right|\right|\gamma} $$
Il margine funzionale dell'iperpiano canonico vale
$$ f_c(x_m) = \frac{f(x_m)}{\left|\left|w\right|\right|\gamma} = \frac{\left|\left|w\right|\right|\gamma}{\left|\left|w\right|\right|\gamma} = 1 $$
Il margine geometrico dell'iperpiano canonico vale
$$ \gamma_c = \frac{f(x_m)}{\left|\left|w_c\right|\right|} = \frac{f(x_m)}{\left|\left|\nicefrac{w}{f(x_m)}\right|\right|} = \frac{1}{\left|\left|w\right|\right|} $$
D'ora in avanti considereremo solo iperpiani canonici, che sono gli iperpiani con margine funzionale unitario e margine geometrico $\nicefrac{1}{\left|\left|w\right|\right|}$\,. Esprimiamo allora, ciò che vogliamo: vogliamo massimizzare il margine
$$ w^* = \argmax_{w\in \mathbb{R}^N} \frac{1}{\left|\left|w\right|\right|} = \argmin_{w\in \mathbb{R}^N} \left|\left|w\right|\right| $$
mantenendo una classificazione corretta per tutti i punti (siamo nel caso in cui il dataset sia linearmente separabile)
$$ yw^Tx\geq 1\hspace{0.5in} \forall(x,y)\in D $$
Deve valere $yw^Tx\geq 1$ perché, per i punti sul margine in una delle due direzioni vale, come abbiamo mostrato prima, $|yw^Tx_m| = 1$\,. Inoltre, il segno di $f(x_m)$ deve essere lo stesso di $y$ perché la classificazione sia corretta, quindi
$$ yw^Tx_m = \begin{cases}
(-1)\cdot (-1) & y = -1 \\
(+1)\cdot (+1) & y = +1 
\end{cases}\Rightarrow yw^Tx_m = 1 $$
Per i punti oltre il margine deve valere $|w^Tx|>1$, quindi $ yw^Tx > 1$. In generale arriviamo a dire che per tutti i punti vale $ yw^Tx\geq 1 $. Possiamo esprimere il tutto come un problema di ottimizzazione quadratica vincolata 
$$ w^* = \argmin_{w\in \mathbb{R}^N} \left|\left|w\right|\right|\hspace{0.2in}:\hspace{0.2in}yw^Tx\geq 1\hspace{0.1in} \forall(x,y)\in D  $$
Risolvendo con KKT otteniamo che il vettore ottimo è determinato solo dai punti sul margine: questi saranno chiamati \textit{support vector}. La soluzione ottima è descritta da un vettore di pesi $\alpha^*$ nulli per punti che non sono support vector
$$ f_{\alpha}(x) = \sum_{(x_i,y_i)\in D} y_i\alpha_ix_i^Tx = \sum_{(x_i,y_i)\in D\ :\ x_i \in \mathcal{SV}} y_i\alpha_ix_i^Tx  $$
$$ w^* = \sum_{(x_i,y_i)\in D\ :\ x_i \in \mathcal{SV}} y_i\alpha_ix_i $$
$$  $$
Il bound sul rischio statistico che deriva dal teorema di Vapnik garantisce che
$$ P \left( er(f) \leq \tilde{er}(f) + \sqrt{\frac{h\left( \ln{\frac{2n}{h}} + 1\right) - \ln{(\nicefrac{\delta}{4})}}{n}}\ \right) \geq 1-\delta $$
Dove $n$ è la dimensione del training set e $h$ è la dimensione di Vapnik-Chervonenkis. Nel caso di SVM, se vale
$$ \left|\left|x\right|\right| \leq R \hspace{0.8in} \forall x \in X $$
$$ \left|\left|w\right|\right| \leq \Lambda \hspace{0.5in} \forall w : w\text{ is valid} $$
Allora vale 
$$ h = R^2\Lambda^2$$
Una prospettiva di questo risultato è che l'algoritmo, minimizzando la norma del vettore $w$ sta anche minimizzando la dimensione di VC agendo sul termine $\Lambda$.

\subsubsection{Soft-margin}
In contrapposizione a \textit{hard-margin} SVM, le SVM \textit{soft-margin} permettono una violazione del vincolo di margine. Introducendo delle \textit{slack variables} $\xi$, il problema risulta vincolato dalle condizioni
$$ y_iw^Tx_i\geq1-\xi_i \hspace{0.5in}\forall(x_i,y_i)\in D $$
Per ottenere una classificazione il più corretta possibile, le violazioni del vincolo di margine devono essere nella minor misura possibile, quindi minimizziamo anche rispetto alle slack
$$ w^* = \argmin_{w\in \mathbb{R}^N} \left|\left|w\right|\right| + C\sum_{(x_i,y_i)}\xi_i$$
Dove $C$ è un parametro di trade-off che regola la tolleranza verso la violazione del vincolo di margine: $C\rightarrow+\infty$ corrisponde al SVM hard-margin.

\subsubsection{Kernel trick}
Per apprendere classi non linearmente separabili, possiamo pensare di mappare l'input space in uno spazio a maggiore dimensionalità con una trasformazione non lineare: per il teorema di Cover, è più probabile ottenere una separazione lineare in uno spazio a maggior dimensionalità. Consideriamo, quindi, una mappa
$$ \Phi: \mathbb{R}^d \rightarrow \mathbb{R}^m\ |\ d \ll m $$
Possiamo applicare SVM nello spazio $\mathbb{R}^m$, ottenendo l'iperpiano $w^*$ e la funzione $f_{\alpha^*}$ 
$$ w^* = \sum_{(x_i,y_i)\in\mathcal{SV}} y_i\alpha_i^*\Phi(x_i) $$
$$ f_{\alpha^*}(x) = \sum_{(x_i,y_i)\in\mathcal{SV}} y_i\alpha_i^*\Phi(x_i)^T\Phi(x)  $$
I prodotti interni $\Phi(x_i)^T\Phi(x)$ hanno complessità lineare nella dimensione $m$ dello spazio di arrivo, che è molto alta. Però, è l'unica operazione che dobbiamo fare sugli input: cerchiamo una funzione \textit{kernel} che implementi il prodotto interno nello spazio ad alta dimensionalità
$$ K_\Phi: \mathbb{R}^d\times\mathbb{R}^d \rightarrow \mathbb{R}, (x,y) \mapsto \Phi(x)^T\Phi(y) $$
In questo modo, possiamo implementare la separazione in uno spazio a dimensionalità elevata con complessità computazionale determinata dalla funzione kernel
$$ f_{\alpha^*}(x) = \sum_{(x_i,y_i)\in\mathcal{SV}} y_i\alpha_i^*K_\Phi(x_i,x)  $$
Alcuni kernel molto utilizzati sono i kernel polinomiali, che implementano il prodotto interno nello spazio di tutti i polinomi dell'input di grado minore o uguale a $n\in\mathbb{N}^o$
$$ K_n(x,y) = (1+x^Ty)^n $$
e i kernel gaussiani (di parametro $\sigma > 0$), che implementano il prodotto interno in uno spazio di dimensionalità infinita
$$ K_\sigma(x,y) = e^{-\frac{\left|\left|x-y\right|\right|^2}{2\sigma^2}} $$

\section{Metriche di Valutazione}
\begin{figure}\centering
	\label{fig:classcases}
	\caption{Possibili predizioni di un classificatore binario in relazione alla classe reale}\ \newline
	\begin{tabular}{cc|cc}
		\multicolumn{2}{c}{} & \multicolumn{2}{c}{\it label} \\
		\multirow{3}{*}{\begin{turn}{90}\it output\end{turn}} & & P & N \\\cline{2-4}
		& P & True Positive & False Positive \\
		& N & False Negative & True Negative 
	\end{tabular}
\end{figure}
I classificatori binari (molto usati in bioinformatica) possono predire due classi: positivo o negativo. In base al valore reale dell'esempio, si possono distinguere quattro casi: vero positivo, falso positivo, vero negativo e falso negativo (figura~\ref{fig:classcases}). Si definiscono le seguenti misure:\begin{itemize}
	\item Precisione: rapporto tra il numero di veri positivi e il numero di predizioni positive. Esprime la probabilità che una predizione positiva sia corretta $$ \frac{TP}{TP+FP} = P( \text{label}=P\ |\ \text{output}=P ) $$
	\item Accuratezza: percentuale di predizioni corrette. Esprime la probabilità che una predizione sia corretta $$ \frac{TP+TN}{TP+FP+TN+FN} = P( \text{label}=\text{output} ) $$
	\item Sensitività (o \textit{recall}): rapporto tra il numero di veri positivi e il numero di positivi reali. Esprime la probabilità che un positivo sia correttamente riconosciuto $$ \frac{TP}{TP+FN} = P( \text{output}=P\ |\ \text{label}=P ) $$
	\item Specificità: rapporto tra il numero di veri negativi e il numero di negativi reali. Esprime la probabilità che un negativo sia correttamente riconosciuto $$ \frac{TN}{TN+FP} = P( \text{output}=N\ |\ \text{label}=N ) $$
	\item $F_1$-score: media armonica tra precisione e recall. Spesso usata in machine learning $$ \frac{2}{\frac{1}{\text{recall}}+\frac{1}{\text{precision}}} = \frac{2\,TP}{2\,TP+FP+FN} $$
\end{itemize}

\subsection{ROC}
\begin{figure} \centering
	\caption{Esempio di curva ROC: sulla sinistra sono riportate le distribuzioni dei reali negativi $H_0$ e positivi $H_1$\,, nonché un esempio di valore di soglia (in rosso) e sulla destra la curva ROC ottenuta facendo variare il valore della soglia}
	\label{fig:roc}
	\ \newline
	\includegraphics[width=\textwidth,trim={0 0.3in 0 0},clip]{img/roc.png}
\end{figure}
La curva ROC (\textit{Receiver Operating Characteristic}) è un grafico che mostra le performance di un classificatore binario al variare della sua soglia di discriminazione: sull'asse delle ordinate è riportata la sensitività e sull'asse delle ascisse 1 meno la specificità (ovvero, la percentuale di veri positivi e la percentuale di falsi positivi).

Se il classificatore predice come negativi i valori sotto la soglia e come positivi i valori sopra la soglia. Con una soglia massima tutti gli esempi vengono classificati come negativi: la sensitività varrà $0$ e la specificità $1$. Con una soglia minima tutti gli esempi vengono classificati come positivi: la sensitività varrà $1$ e la specificità $0$. Per cui una curva ROC ha gli estremi nei punti $(0,0)$ e $(1,1)$ (figura~\ref{fig:roc}).

Un classificatore perfetto avrebbe sensitività e specificità unitarie: passerebbe, dunque, per il punto $(0,1)$\,. Solitamente, però, tale classificatore non è possibile a causa della casualità nei dati. Per misurare la bontà di un classificatore binario possiamo osservare quanto la curva ROC è simile alla curva del classificatore perfetto: utilizziamo a questo proposito l'area sotto la curva ROC (AUROC), che varrà $1$ per il classificatore perfetto e $0.5$ per il classificatore casuale uniforme (la cui curva ROC è la bisettrice del quadrante).

\subsection{PRC}
Consideriamo ciò che succede in una curva ROC per dati sbilanciati: ovvero, quando il numero di reali positivi è molto minore del numero di reali negativi.

Il rapporto di falsi positivi (1 meno la specificità), essendo i reali negativi molti, tende a non essere molto influenzato dal numero di falsi positivi: la misura tende a scendere molto lentamente al calare della soglia.

Al contrario, il rapporto di veri positivi (la sensitività), essendo i reali positivi pochi, tende a salire molto velocemente al calare della soglia.

Un classificatore che cerca di massimizzare sensitività e specificità su un insieme di dati altamente sbilanciato tenderà ad avere un elevato numero di falsi positivi, perché ad un grande guadagno nell'aumento di veri positivi per la sensitività non corrisponde una perdita altrettanto pesante in termini di specificità.

\begin{figure}
	\centering
	\caption{Confronto tra le curve PR e ROC per una famiglia di classificatori binari. Osservando la curva ROC il classificatore migliore sembra quello di parametro $w=1000$ (curva nera), anche se molto simile ai concorrenti. Dalla PRC, invece, risulta evidente come tale classificatore sia uno dei peggiori, mentre risultano migliori i classificatori ottenuti con parametri intermedi (con $w=1$ risulta pessimo per entrambe)}
	\label{fig:prcroc}
	\ \newline
	\includegraphics[width=0.9\textwidth,trim={0 0.1in 0 0.1in},clip]{img/prcroc.png}
\end{figure}
Per questo, su dati sbilanciati come sono i dati in bioinformatica, si preferisce utilizzare la PRC (\textit{Precision-Recall Curve}), che riporta sulle ordinate la precisione e sulle ascisse la recall (figura~\ref{fig:prcroc}): la precisione, al contrario della specificità, è molto influenzata dal numero di falsi positivi e per niente influenzata dal numero di negativi reali.

L'ottimo per la curva precision-recall sarebbe il punto $(1,1)$ e un classifcatore casuale uniforme ha come curva il segmento tra $(0,1)$ e $(1,0)$\,. Anche nel caso della PRC, possimo utilizzare l'area sotto la curva (AUPRC) come indicatore della performance del classificatore.
