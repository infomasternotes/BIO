\chapter{Biologia Computazionale}
I problemi della bioinformatica sono: la ricerca delle strutture del DNA, la predizione delle proprietà delle proteine (proteomica funzionale), la predizione di dati fenotipici a partire da dati genotipici (es. genomica medica).

Tipicamente, sono problemi risolti con tecniche di \textit{machine learning}, perché sono troppo complessi per essere trattati con soluzioni teoriche. Inoltre, sono generalmente disponibili grandi moli di dati. I problemi della bioinformatica rappresentano una sfida per le tecniche di machine learning in quanto: i campioni sono tipicamente molto rumorosi e poco etichettati, inoltre sia gli input sia gli output possono essere dati strutturati (array, alberi, grafi, \dots ) e composti da diversi tipi di dato elementari.

A questi problemi, si aggiungono anche i problemi di basso livello come l'elaborazione delle immagini e l'elaborazione di sequenze di dati.

Alcune applicazioni importanti sono nell'ambito della medicina genomica, che si basa sullo studio dei caratteri fenotipici e genotipici del paziente per determinare i trattamenti più adeguati. Diventa, così, possibile tutto un insieme di tecniche di diagnostica e terapia di precisione, che possono eventualmente servirsi di tecnologie all'avanguardia come quelle per l'editing del DNA.

\subsubsection{Cell variables}
Abbiamo detto che i problemi della biologia computazionale sono spesso formulati nei termini di problemi di apprendimento supervisionato: predire il rischio di una malattia a partire da una sequenza di DNA non è, tuttavia, una funzione semplice da apprendere.

Spesso in bioinformatica si utilizzano delle variabili intermedie, chiamate \textit{cell variable}, che rappresentano informazioni sulle caratteristiche biochimiche di una cellula. Possiamo separare il problema di apprendimento in due step: il primo si occupa della predizione delle cell variable a partire dalla sequenza di DNA e il secondo si occupa di predire il rischio della patologia a partire dalle cell variable.

\section{GWAS}
\afterpage{\begin{landscape}\thispagestyle{empty}\newgeometry{left=0.25in,right=0.25in}
\begin{figure} \centering
		\caption{Diagramma dei GWAS: per ogni cromosoma e per ogni suo gene sono indicati i tratti associati. \newline Il browser web \href{http://www.ebi.ac.uk/gwas/diagram}{\ding{238}} permette di evidenziare i tratti a cui si è interessati \newline\ }
		\label{fig:gwas}
		\ \newline
		\includegraphics[width=\textwidth]{img/gwas.pdf}
\end{figure}
\end{landscape}}
I \textit{Genome-Wide Association Studies} hanno raccolto dati sulla correlazione tra tratti fenotipici e genotipici: ad ogni patologia viene associata la significatività statistica delle mutazioni in loci particolari con tecniche di acquisizione basate su sequenziamento e microarray.

I GWAS hanno vari difetti. Innanzitutto, è molto difficile stabilire una significatività statistica di una mutazione potenzialmente causativa rispetto ad una variazione del rischio per una particolare patologia: in ogni caso, comunque, si tratta di una misura della correlazione e non della causalità.
Inoltre, i GWAS restituiscono solitamente un grande numero di mutazioni potenzialmente causative (quindi, molti falsi positivi) e i ricercatori si sbilanciano rispetto ai preconcetti che hanno in materia.

\section{Conservazione Evolutiva}
\afterpage{\begin{landscape}\thispagestyle{empty}\newgeometry{left=0in,right=0in}
\begin{figure} \centering
	\caption{Score di conservazione evolutiva del gene GATA3 forniti dal ECR browser\hspace{0.1in}\href{https://ecrbrowser.dcode.org/xB.php?db=hg19&location=chr10:8096667-8116494}{\ding{238}}\,. Si notino le regioni colorate in blu, conservate in tutte le specie confrontate \newline\ }
	\label{fig:ecr}
	\ \newline
	\includegraphics[width=\textwidth]{img/ecr-gata3.png}
\end{figure}
\end{landscape}}
Molti metodi sono basati sulla conservazione evolutiva del genoma (figura~\ref{fig:ecr}): se l'evoluzione si muove sotto la spinta di mutazioni casuali e della selezione delle varianti più adatte, allora, una mutazione in una regione conservata è molto probabilmente deleteria.

Una mutazione è deleteria quando influisce negativamente sulla capacità riproduttiva dell'individuo: una mutazione deleteria è patogenica se è causa di una malattia.

\section{CADD}
\afterpage{\begin{landscape}
		\thispagestyle{empty}
		\newgeometry{left=0in,right=0in}
		\begin{figure} \centering
			\caption{Classficazione di varianti in base allo scaled C-score: a ogni indice è associata una probabilità per ogni classe di mutazione (indicate con i colori in legenda)}
			\label{fig:cadd}
			\ \newline
			\includegraphics[width=\textwidth,trim={ 0.3in 5.1in 1.8in 3.5in},clip]{img/cadd.pdf}\begin{flushleft}
				\hspace{0.1\textwidth}
				\includegraphics[height=0.8\textwidth,trim={6.75in 3in 0.2in 3.6in},clip,angle=270]{img/cadd.pdf}
			\end{flushleft}
		\end{figure}
\end{landscape}}
CADD (\textit{Combined Annotation Dependent Depletion}) è uno strumento per la valutazione della \textit{deleteriousness} delle varianti a singolo nucleotide e delle mutazioni per inserimento o delezione (\textit{indel}) nel genoma umano.

Esistono molti metodi per l'annotazione delle varianti, ma molti tendono a sfruttare solamente un tipo di informazione o si riferiscono ad un tipo particolare di mutazione. CADD è un framework unico che integra più di 60 modalità di annotazione in una sola metrica, confrontando varianti reali con varianti simulate.

I \textit{C-score} così ottenuti sono molti correlati alla patogenicità delle varianti sia in regioni \textit{coding} che \textit{non-coding}, che hanno riscontri in effetti regolatori verificati sperimentalmente.

\subsubsection{Dati per l'addestramento}
Il training del sistema viene effettuato su un dataset di 15 milioni di esempi positivi reali e 15 milioni di esempi negativi simulati.

Gli esempi positivi sono ottenuti come le varianti alleliche che hanno una frequenza superiore al 95\%.
Per gli esempi negativi, invece, si è ricavato un modello di progenitore dei primati: ogni allele di questa sequenza viene permutato secondo la distribuzione di probabilità ottenuta dal dataset.

\subsubsection{Scores}
Le feature in ingresso sono le 63 modalità di annotazione, integrate con un piccolo numero di termini di interazione. Lo score in uscita (\textit{raw C-score}) è stato calcolato per tutte le possibili 8.6~miliardi di varianti del genoma umano di riferimento: per rendere il punteggio più leggibile, il range è stato riscalato tra 1 e 99, ottenendo uno \textit{scaled C-score}.

I \textit{raw CADD score} sono il risultato diretto del modello e indicano, quando maggiori, una maggiore probabilità che la mutazione sia deleteria. I punteggi raw hanno un valore relativo e maggiore risoluzione, ma non hanno alcun valore assoluto.

Invece, gli \textit{scaled CADD score} sono ottenuti come la scalatura per ordini di grandezza dei punteggi grezzi: le varianti al primo decile sono CADD-10, quelle al primo percentile sono CADD-20, quelle al primo millile CADD-30, \dots Questi punteggi hanno, quindi, un'interpretazione immediata e sono confrontabili anche tra diverse versioni di CADD, ma hanno una risoluzione inferiore.

\subsubsection{Versioni}
CADD 1.0 utilizza come algoritmo di apprendimento un SVM lineare, mentre da CADD 1.1 viene utilizzato un algoritmo di regressione logistica: inoltre, CADD 1.1 estende leggermente l'insieme di annotazioni utilizzate. In CADD 1.3 è stato aggiornato il training set.

\begin{multicols}{2}
\section{DeepSEA}
\textit{DeepSEA} è un sistema basato su deep learning per la predizione degli effetti degli SNP in regioni non-codificanti.

Come \textit{cell variable}, vengono predette delle feature epigenetiche come siti di binding dei fattori di trascrizione, sensitività alla DNasi, \dots per poi predire gli effetti sulla cromatina e ordinare le varianti.

L'analisi è rivolta alle regioni non-coding perché più del 96\% degli SNP nelle patologie tumorali sono in tali regioni: tuttavia, solo poche sono causative. Varianti in regioni non-codificanti possono essere deleterie interrompendo o creando motivi nelle sequenze del DNA che inibiscono o abilitano il legame di fattori di trascrizione o il binding di miRNA; inoltre, mutazioni negli introni possono modificare lo splicing.

\columnbreak\ \\
\includegraphics[width=0.5\textwidth]{img/deepsea.jpg}
\end{multicols}

\subsubsection{Variabili}
L'input della rete è costituito dalla sequenza target e dalla regione circostante: viene stabilita una dimensione di finestra di 1000 nucleotidi, di cui i 200 nucleotidi centrali rappresentano la sequenza target. La codifica è binaria a 4-bit e ogni base viene codificata con un uno sul bit corrispondente e con uno zero sugli altri tre bit: una sequenza din $n$ basi diventa, quindi, una matrice $4\times n$ di bit.

Le cell variable intermedie sono l'ipersensitività alla DNasi, i legami di fattori di trascrizione e i marker istonici. Esse sono predette per ogni finestra di 200 basi dalla CNN.

\noindent
Il predittore finale utilizza le cell variable per stimare l'effetto della variante.

\subsubsection{Rete Convoluzionale}
Il primo layer della rete neurale di DeepSEA è composto da una matrice $D\times W$ di neuroni, dove il neurone $h_{d,w}$ si occupa di rilevare il motivo $d$ nella finestra $w$\,. I motivi target hanno una lunghezza di 6 basi: la larghezza della finestra è 6, con un \textit{hop size} di 1. Quindi il numero di finestre $W$ è cinque unità in meno rispetto alla lunghezza della sequenza (no padding).

Il numero di motivi è virtualmente variabile, ma tutti i neuroni $\graffe{h_{d,w}}_{w=1}^{W}$ cercano di rilevare lo stesso motivo: non ha senso usare uno strato completamente connesso, ma si sfrutta il parameter sharing degli strati convoluzionali per facilitare il training e ridurre il numero di parametri. Ogni motivo sarà, quindi, rilevato da uno specifico kernel $4\times 6$.

Ricapitolando: il primo layer di DeepSEA è uno strato convoluzionale con $D$ kernel $4\times 6$. Dopodiché viene applicata una funzione di attivazione ReLU e max pooling con stride di larghezza 4. A questo, seguono altri due strati convoluzionali con ReLU e max pooling. Infine, si trova uno strato completamente connesso con attivazione logistica e uno strato finale di output con 919 neuroni e attivazione logistica.

\subsubsection{Boosting}
L'output della rete convoluzionale restituisce 919 feature: da queste si calcolano le differenze assolute ( $P(ref)-P(alt)$ ) e relative ( $\log{\nicefrac{P(ref)}{P(alt)}}$ ) e si integrano gli score di conservazione evolutiva. Tutte queste feature (1842) sono preprocessate calcolandone il valore assoluto e sono standardizzate: il predittore viene ottenuto con un algoritmo di boosting per la regressione logistica .

\section{HyperSMURF}
\begin{figure} \centering
	\caption{Rappresentazione schematica e pseudocodice dell'algoritmo hyperSMURF: i dati vengono partizionati, bilanciati e usati come training set degli algoritmi di apprendimento random forest, le cui predizioni vengono combinate}
	\label{fig:hypersmurf}
	\includegraphics[width=0.75\textwidth,trim={0 0.75in 0 0.75in},clip]{img/hypersmurf.png}
	\lstinputlisting{cod/hypersmurf.code}
\end{figure}
HyperSMURF (\textit{Hyper-ensamble of SMote Undersampled Random Forests}) è un metodo di \textit{hyper-esamble} per la predizione delle varianti deleterie in regioni non-codificanti, che affronta direttamente il problema dello sbilanciamento dei dati. L'algoritmo è descritto in figura~\ref{fig:hypersmurf}\,.

\subsubsection{SMOTE}
I dati disponibili nel training set, come spesso accade in bioinformatica, sono estremamente sbilanciati: essi vengono ribilanciati sovracampionando la classe minoritaria e sottocampionando la classe maggioritaria.

La tecnica usata per il sovracampionamento della classe minoritaria è SMOTE (\textit{Synthetic Minority Oversampling TEchnique}). Per creare un campione sintetico da un campione natutrale $x$\,, viene scelto con probabilità uniforme $x_{knn}$ uno dei $k$ nearest neighbor del campione e un numero $\alpha$ tra 0 e 1 (con probabilità uniforme): il campione sintetico è dato dalla combinazione convessa dei due campioni $x$ e $x_{knn}$ con coefficienti $\alpha$ e $1$-$\alpha$\,.
$$ \text{SMOTE}(x) := (1-\alpha)\,x + \alpha\, x_{knn}\ :\ \alpha \leftarrow \text{rand}(0,1),\ x_{knn} \leftarrow \text{rand}(\text{KNN}(x)) $$

\subsubsection{Hyper-ensemble}
HyperSMURF è un metodo di hyper-ensemble. Un metodo di \textit{ensemble} è un algoritmo di apprendimento in cui vengono addestrati più modelli: in fase di predizione, le etichette predette dai vari modelli vengono combinate per ottenere la predizione dell'ensemble. Nei metodi di \textit{hyper-ensemble} vengono addestrati più ensemble.

HyperSMURF utilizza un hyper-ensemble di random forest: ogni random forest è un ensemble di \textit{decision tree}. Lo score predetto dall'hyper-ensemble è la media degli score predetti dalle random forest.

\subsubsection{Dati}
L'algoritmo è stato sviluppato su due diversi dataset: uno relativo a patologie Mendeliane e uno relativo a dati GWAS.

I dati relativi a patologie Mendeliane sono stati estratti da banche dati pubbliche e sono score di conservazione evolutiva, feature relative alla trascrizione e alla regolazione e altre caratteristiche epigenomiche.

I dati GWAS, invece, sono mappati dalla sequenza di DNA in 1842 feature tramite la rete convoluzionale di DeepSEA e l'integrazione di score di conservazione evolutiva.
\begin{figure} \centering
	\caption{Confronto delle performance di HyperSMURF con gli altri metodi allo stato dell'arte sul dataset di patologie Mendeliane (a) e GWAS (b). Confronto delle performance di HyperSMURF con CADD al variare dello sbilanciamento del dataset\newline\ }
	\label{fig:hycompare}
	\includegraphics[width=0.66\textwidth]{img/hycomparePRC.jpg}
	\includegraphics[width=0.33\textwidth]{img/hycompareIMB.jpg}
\end{figure}
